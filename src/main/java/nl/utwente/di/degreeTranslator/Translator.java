package nl.utwente.di.degreeTranslator;

public class Translator {
    public double getDegreeTranslation(String degree) {
        return Double.parseDouble(degree) * 1.8 + 32;
    }
}